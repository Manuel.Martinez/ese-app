package com.example.alfredo.eseapp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.chootdev.csnackbar.Align;
import com.chootdev.csnackbar.Duration;
import com.chootdev.csnackbar.Snackbar;
import com.chootdev.csnackbar.Type;
import com.example.alfredo.eseapp.R;
import com.example.alfredo.eseapp.data.model.Institucion;
import com.example.alfredo.eseapp.data.model.LoginPost;
import com.example.alfredo.eseapp.data.webservice.ApiInterface;
import com.example.alfredo.eseapp.data.webservice.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;


public class Login extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private SharedPreferences prefs;

    Spinner spinnerInsti;
    TextInputEditText correo;
    TextInputEditText contrasena;
    Button btnLogin;
    ServiceGenerator userService;
    String URL_BASE="https://www.ese.school/api/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        correo = (TextInputEditText) findViewById(R.id.editTextEmail);
        contrasena = (TextInputEditText) findViewById(R.id.editTextPassword);
        btnLogin = (Button) findViewById(R.id.loginButton);
        spinnerInsti = (Spinner) findViewById(R.id.spinner);

        prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE); // guarda preferencias de usuario
        retrofitGetInstituciones();
    }

     //Button
    public void iniciarSesion(View view)
    {
        String username = correo.getText().toString().trim();
        String password = contrasena.getText().toString().trim();
        String school = "72";

        if (validaciones())
        {
            retrofitLogin(username,password,school);
        }
        else
        {
            mensajes("Error al iniciar sesión");
        }

    }


    private void goToMain()
    {
        Intent intent = new Intent(this, MainActivity.class);
        //con esta linea,al iniciar sesion, no regresará a la pantalla de login
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void retrofitLogin(String username, String password, String school)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_BASE)
               // .baseUrl("http://192.168.0.16:8080")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface interfaces = retrofit.create(ApiInterface.class);

       // Call<LoginPost> call = (Call<LoginPost>) new LoginPost();
        Call<LoginPost> call = interfaces.loginUser(username, password, school);
        call.enqueue(new Callback<LoginPost>() {
            @Override
            public void onResponse(Call<LoginPost> call, Response<LoginPost> response) {
                int statusCode = response.code();
                LoginPost user = response.body();
                //Log.wtf("RESPONSE",user.getUserdata().getTeacher_name());
                goToMain();

            }

            @Override
            public void onFailure(Call<LoginPost> call, Throwable t) {
                mensajes("Error al iniciar sesión");
            }
        });

    }


    public boolean validaciones()
    {
        boolean valid = true;

        String email = correo.getText().toString();
        String password = contrasena.getText().toString();
        //correo
        if (email.isEmpty()) {
            correo.setError(getString(R.string.error_field_required));
            correo.requestFocus();
            valid = false;
        } else {
        correo.setError(null);
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            correo.setError(getString(R.string.error_invalid_user_id));
            correo.requestFocus();
            valid = false;
        } else {
            correo.setError(null);
        }

        if(correo.getText().toString().length()==0){
            correo.setError(getString(R.string.error_field_required));
            correo.requestFocus();
        } else {
            correo.setError(null);
        }


        //Contraseña
        if (password.isEmpty()) {
            contrasena.setError(getString(R.string.error_invalid_password));
            contrasena.requestFocus();
            valid = false;
        } else {
            contrasena.setError(null);
        }

        if (password.length() <= 5) {
            contrasena.setError("La contraseña debe tener al menos 6 carácteres ");
            contrasena.requestFocus();
            valid = false;
        } else {
            contrasena.setError(null);
        }

        if(contrasena.getText().toString().length()==0){
            contrasena.setError(getString(R.string.error_field_required));
            contrasena.requestFocus();
        } else {
            contrasena.setError(null);
        }


        return valid;
    }



    private void mensajes(String mensaje) {
        View view = getLayoutInflater().inflate(R.layout.activity_login, null);

        Snackbar.with(this,null)
                .type(Type.ERROR)
                .message(mensaje)
                .duration(Duration.LONG)
                .fillParent(true)
                .textAlign(Align.CENTER)
                .show();

    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void retrofitGetInstituciones ()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                // .baseUrl("http://192.168.0.16:8080")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface interfaces = retrofit.create(ApiInterface.class);

        // Call<LoginPost> call = (Call<LoginPost>) new LoginPost();
        Call<List<Institucion>> call = interfaces.schools();
        call.enqueue(new Callback<List<Institucion>>() {
            @Override
            public void onResponse(Call<List<Institucion>> call, Response<List<Institucion>> response) {
                int statusCode = response.code();
                List<Institucion> instituciones = response.body();
                //Log.wtf("RESPONSE",user.getUserdata().getTeacher_name());

                mensajes("Instituciones recibidas con exito!");

            }

            @Override
            public void onFailure(Call<List<Institucion>> call, Throwable t) {
                mensajes("Error al iniciar sesión");
            }
        });

    }

}
