package com.example.alfredo.eseapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Institucion {
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("school_logo")
    @Expose
    private String schoolLogo;
    @SerializedName("school_url")
    @Expose
    private String schoolUrl;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("school_mun")
    @Expose
    private String schoolMun;
    @SerializedName("school_depto")
    @Expose
    private String schoolDepto;

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolLogo() {
        return schoolLogo;
    }

    public void setSchoolLogo(String schoolLogo) {
        this.schoolLogo = schoolLogo;
    }

    public String getSchoolUrl() {
        return schoolUrl;
    }

    public void setSchoolUrl(String schoolUrl) {
        this.schoolUrl = schoolUrl;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolMun() {
        return schoolMun;
    }

    public void setSchoolMun(String schoolMun) {
        this.schoolMun = schoolMun;
    }

    public String getSchoolDepto() {
        return schoolDepto;
    }

    public void setSchoolDepto(String schoolDepto) {
        this.schoolDepto = schoolDepto;
    }

    @Override
    public String toString()
    {
        return  schoolName;
    }

}
