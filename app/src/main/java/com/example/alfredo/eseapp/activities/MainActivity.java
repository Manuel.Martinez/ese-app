package com.example.alfredo.eseapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chootdev.csnackbar.Align;
import com.chootdev.csnackbar.Duration;
import com.chootdev.csnackbar.Snackbar;
import com.chootdev.csnackbar.Type;
import com.example.alfredo.eseapp.ChatMessage;
import com.example.alfredo.eseapp.R;
import com.example.alfredo.eseapp.data.adapter.ChatMessageAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

private TextView txvResult;
private LinearLayout contenedor;
private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txvResult = (TextView) findViewById(R.id.txvResult);
        contenedor = (LinearLayout) findViewById(R.id.contenedor);
        editText = (EditText) findViewById(R.id.editText);
    }

    //Speech to Text

    public void getSpeechInput(View view) {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            view = getLayoutInflater().inflate(R.layout.activity_main, null);

            Snackbar.with(this,null)
                    .type(Type.SUCCESS)
                    .message("¡Se inició sesión correctamente!")
                    .duration(Duration.SHORT)
                    .fillParent(true)
                    .textAlign(Align.CENTER)
                    .show();

        }
    }
    //acciones del speech to text
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txvResult.setText(result.get(0));

                    if (result.contains("video")) {
                        videoYoutube();
                    }

                    if(result.contains("imagen"))
                    {
                        goImagen();
                    }

                    if (result.contains("lista"))
                    {
                        goLista();
                    }

                    if(result.contains("galería"))
                    {
                        goGaleriaFotos();
                    }

                }
               
                break;
        }
    }

    private void videoYoutube()
    {
        Intent intent = new Intent(this, Video.class);
        startActivity(intent);
    }

    private void goImagen()
    {
        Intent intent = new Intent(this, Imagen.class);
        startActivity(intent);
    }

    private void goLista()
    {
        Intent intent = new Intent(this, Lista.class);
        startActivity(intent);
    }

    private void goGaleriaFotos()
    {
        Intent intent = new Intent(this, Galeria.class);
        startActivity(intent);
    }


        //========== Mostrar y ocultar el teclado y editText =======================
    public void showKeyboard()
    {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void closeKeyboard()
    {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }


    public void mostrarElementos(View view)
    {
        showKeyboard();
        contenedor.setVisibility(View.VISIBLE);
    }

    public void esconderElementos(View view)
    {
        contenedor.setVisibility(View.INVISIBLE);
        closeKeyboard();
    }

    public void guardarTexto(View view) {

        String dato = editText.getText().toString();
        txvResult.setText(dato);
        resultadoDePalabras();
       // editText.setInputType(InputType.TYPE_NULL);
/*
        if (editText.getText().toString().trim().equals("")) {
            Toast.makeText(MainActivity.this, "Please input some text...", Toast.LENGTH_SHORT).show();
        } else {
            //add message to list
            ChatMessage chatMessage = new ChatMessage(editText.getText().toString(), myMessage);
            list_chat.add(chatMessage);
            editText.setText("");
            if (myMessage) {
                myMessage = false;
            } else {
                myMessage = true;
            }
        }*/
    }


    //========== Acciones del teclado  =======================

    public void resultadoDePalabras()
    {
        if(txvResult.getText().toString().equals("Video"))
        {
            videoYoutube();
        }
        if (txvResult.getText().toString().equals("Imagen"))
        {
            goImagen();
        }
        if (txvResult.getText().toString().equals("Lista"))
        {
            goLista();
        }
        if (txvResult.getText().toString().equals("Galería"))
        {
            goGaleriaFotos();
        }

    }

}