package com.example.alfredo.eseapp.data.webservice;

import com.example.alfredo.eseapp.data.model.LoginPost;
import com.example.alfredo.eseapp.data.model.Institucion;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("loginusers")
    Call<LoginPost> loginUser(@Field("username") String username,
                              @Field("password") String password,
                              @Field("school")String  school);

    @GET("schools")
    Call<List<Institucion>> schools();


}
