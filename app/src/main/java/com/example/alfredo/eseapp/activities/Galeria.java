package com.example.alfredo.eseapp.activities;

import android.os.Bundle;


import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.example.alfredo.eseapp.Img;
import com.example.alfredo.eseapp.R;
import com.example.alfredo.eseapp.data.adapter.galleryAdapter;

import java.util.ArrayList;

public class Galeria extends AppCompatActivity {

    private final Integer images_id [] =
    {
            R.drawable.imagen1,
            R.drawable.imagen2,
            R.drawable.imagen3,
            R.drawable.imagen4,
            R.drawable.imagen5,
            R.drawable.imagen6
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galeria);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerGallery);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),2);
        recyclerView.setLayoutManager(layoutManager);

        ArrayList<Img> img = prepareData();
        galleryAdapter adapter = new galleryAdapter(getApplicationContext(), img);
        recyclerView.setAdapter(adapter);



    }

    private ArrayList<Img> prepareData()
    {
        ArrayList<Img> theimage = new ArrayList<>();
        for (int i=0; i < images_id.length;i++)
        {
            Img img = new Img();
            img.setImg(images_id[i]);
            theimage.add(img);
        }
        return theimage;
    }
}
