package com.example.alfredo.eseapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.example.alfredo.eseapp.R;

public class Imagen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagen);

        mostrarImagen();
    }
    private void mostrarImagen()
    {
        ImageView imageView = (ImageView)findViewById(R.id.imagen);
        Glide.with(this)
                .load("https://cdn.pixabay.com/photo/2018/04/27/08/56/water-3354067_960_720.jpg")
                .into(imageView);
    }
}
