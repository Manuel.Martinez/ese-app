package com.example.alfredo.eseapp.data.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.alfredo.eseapp.data.model.Institucion;

import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<Institucion> {

    private Context mContext;
    private List<Institucion> mItems;

    public SpinnerAdapter(@NonNull Context context, @NonNull List<Institucion>items) {
        super(context, 0, items);
        mContext = context;
        mItems = items;
    }

    @NonNull
    @Override
    public View getView (int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        View view;
        Institucion spinnerInsti = mItems.get(position);

        if (convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item,parent,false);

        }
        else
        {
            view = convertView;
        }
        TextView textView = (TextView)view.findViewById(android.R.id.text1);
        textView.setText(spinnerInsti.getSchoolName());

        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        return getView(position, convertView, parent);
    }
}
