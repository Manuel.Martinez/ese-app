package com.example.alfredo.eseapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Group {
    @SerializedName("subject_id")
    @Expose
    private String subject_id;
    @SerializedName("subject_title")
    @Expose
    private String subject_title;
    @SerializedName("group_id")
    @Expose
    private String group_id;
    @SerializedName("level_id")
    @Expose
    private String level_id;
    @SerializedName("section_id")
    @Expose
    private String section_id;
    @SerializedName("section_title")
    @Expose
    private String section_title;

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getSubject_title() {
        return subject_title;
    }

    public void setSubject_title(String subject_title) {
        this.subject_title = subject_title;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getLevel_id() {
        return level_id;
    }

    public void setLevel_id(String level_id) {
        this.level_id = level_id;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getSection_title() {
        return section_title;
    }

    public void setSection_title(String section_title) {
        this.section_title = section_title;
    }
}
