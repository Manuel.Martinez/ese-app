package com.example.alfredo.eseapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {
    @SerializedName("teacher_username")
    @Expose
    private String teacher_username;

    public String getTeacher_username() {
        return teacher_username;
    }

    public void setTeacher_username(String teacher_username) {
        this.teacher_username = teacher_username;
    }

    @SerializedName("teacher_name")
    @Expose
    private String teacher_name;

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    @SerializedName("teacher_lastname")
    @Expose
    private String teacher_lastname;

    public String getTeacher_lastname() {
        return teacher_lastname;
    }

    public void setTeacher_lastname(String teacher_lastname) {
        this.teacher_lastname = teacher_lastname;
    }

    @SerializedName("teacher_email")
    @Expose
    private String teacher_email;

    public String getTeacher_email() {
        return teacher_email;
    }

    public void setTeacher_email(String teacher_email) {
        this.teacher_email = teacher_email;
    }

    @SerializedName("teacher_imagen")
    @Expose
    private String teacher_imagen;

    public String getTeacher_imagen() {
        return teacher_imagen;
    }

    public void setTeacher_imagen(String teacher_imagen) {
        this.teacher_imagen = teacher_imagen;
    }
}
