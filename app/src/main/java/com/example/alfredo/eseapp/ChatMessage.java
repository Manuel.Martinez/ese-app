package com.example.alfredo.eseapp;

import android.widget.TextView;

public class ChatMessage {

    public String message;
    public boolean isSend;

    public ChatMessage(String message, boolean isSend) {
        this.message = message;
        this.isSend = isSend;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSend() {
        return isSend;
    }

    public void setSend(boolean send) {
        isSend = send;
    }
}