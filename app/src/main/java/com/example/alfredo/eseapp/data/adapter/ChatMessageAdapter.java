package com.example.alfredo.eseapp.data.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.alfredo.eseapp.ChatMessage;
import com.example.alfredo.eseapp.R;
import java.util.List;


public class ChatMessageAdapter extends BaseAdapter
{
    private List<ChatMessage> list_chat_models;
    private Context context;
    private LayoutInflater layoutInflater;

    public ChatMessageAdapter(List<ChatMessage> list_chat_models, Context context) {
        this.list_chat_models = list_chat_models;
        this.context = context;
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list_chat_models.size();
    }

    @Override
    public Object getItem(int position) {
        return list_chat_models.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null)
        {
            if(list_chat_models.get(position).isSend)
                view = layoutInflater.inflate(R.layout.mensaje_enviado_item,null);
            else
                view = layoutInflater.inflate(R.layout.mensaje_recivido_item,null);

        }
        return view;
    }
}